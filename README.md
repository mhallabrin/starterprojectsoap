# StarterProjectSoap
A starter project for new Soap API Testing project with a SOAP service and client example using the node-soap package and express.


# Introduction
A starter project for Quest Diagnostics to start Soap API Testing.  This project includes a Soap Service for new resources to ramp up on the Soap testing process is using Postman primarily and SoapUI to create the soap messages.

This README will go through StarterProject Soap project and the sample Soap Service.


# Reader knowledge
This readme assumes that the reader has a basic working knowledge of:

  0. Soap, WSDL's and Soap Services
  1. Postman
  2. SoapUI
  3. JavaScript
  4. JSON
  5. Node & NPM
  6. Express


To learn or brush up on the above topic, there are plenty of free videos, training sites and tutorials on the internet.  To begin Soap API testing a resource only requires the basic working compentency and not a deep or advanced understanding.  More complex scenarios and techniques will come with experience with the tools and process.

Also read the readme.md from the StarterProjectRest and it's cooresponding


# Sample Soap Service
Pre-requisite:  Install Node

The Soap Service splits a string message into its components, based on a user defined splitter character.

Example:
The request constains the string "quest:diagnostics:soap:api:testing" with a spltter character of ":".  The response would return a results array as: 'quest', 'diagnostics', 'soap', 'api', 'testing'.


# Install & Start Soap Service
Download the StarterProject and run `npm install` in the project root directory.

Run the server with a command window or terminal by typing and entering  `npm start` at the project root.
To Test:
  0. In browser type: localhost:8000/wsdl?wsdl, and you will see the wsdl in the browser.
  1. To run a quick test of the service use the pre-developed SOAP client by typing `node client.js` (with a command window or terminal at the project root).
  2. Finally use Postman or SoapUI to run the test.


# Take the Soap API Testing Challenge
To learn Soap API Testing the following steps will guide you through creating your first tests using the sample service.  There is an companion "Quest Soap API Testing" video to watch that goes over this same StarterProjecSoap.  Please reread this readme.md doc and understand this project and especially the Reader Knowledge section.

Then try the following steps on your own:
  0. The Splitter Soap service is up and running.
  1. Install:
    a. Postman
    b. Soapui
    c. npm install newman -g
    d. Jenkins (to run your tests in a pipeline)
  2. Review the StarterProjectSoap directories and assets 
  3. Open up Postman and create a new collection
    a. No environment is needed
    b. No need to create Global variables
  4. Create a getWSDL message to run the wsdl
    a. Get / {{url}}?wsdl 
    b. Update the Header to include content-type: text/xml
    c. Add Pre-request Scripts:
      pm.globals.set("url", data.url);
      pm.globals.set("message", data.message);
      pm.globals.set("splitter", data.splitter);
    d. Add a basic 200 response code test
    e. Run test using the Collection and selecting the data file: splitter.json
  5. Create a postSplitter test
    a. Post / {{url}}
    b. Header-> content-type: text/xml
    c. Body -> use soapui to pull in wsdl and create the request message.
    d. Add tests for the responses:

pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});

var jsonObject = xml2Json(responseBody);

console.log('results: ' + jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result)

console.log('result: ' + jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[0])

console.log('item0: ' + data.items[0].item);

pm.test("Validate Split Message", function () {
   pm.expect(jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[0]).to.eql(data.items[0].item);
   pm.expect(jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[1]).to.eql(data.items[1].item);
   pm.expect(jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[2]).to.eql(data.items[2].item);
   pm.expect(jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[3]).to.eql(data.items[3].item);
   pm.expect(jsonObject['soap:Envelope']['soap:Body'].MessageSplitterResponse.result[4]).to.eql(data.items[4].item);
});

  6. Export Collection to StarterPorjectSoap
  7. Run with Newman
    newman run StarterProjectSoap -d splitter.json
  8. Update Jenkinsfile, move to source repository, create pipeline and run tests


# Starting a new Soap API Testing project
Follow the steps below to create a new Soap API Testing project

  0. Get the WSDL and documentation fo the Soap API under test
  1. Recommend the API test code use the same code repository as the Soap Services it's testing
    a. In Soap Service repository, create a new Test directory for the json data and collection files
  2. If the Soap Service SCM repository is not available, create a new SCM repository for your testing JSON
  3. If the Soap project has many services, create seperate folders per Service or some logical organization of the code
    a. However if the services are small, than one Test directory is sufficient
  4. Create JSON data files to meet the needs of the request messages and the response testing
  5. Create the Postman collection and start developing the tests
  6. Use SoapUI or some other utility to create request messages from the provided WSDL
  7. Run tests with the Postman Collection Runner
  8. Export collection and run tests locally with Newman
  9. Update the Jenkinsfile 
  10. Commit all code to SCM
  11. Create pipeline to run tests 


